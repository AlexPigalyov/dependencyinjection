﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoCExample.Entities.Abstract
{
    public interface IBaseEntity
    {
        int Id { get; set; }
        DateTime CreateDate { get; set; }
    }
}
