﻿using IoCExample.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IoCExample.Entities
{
    [Table("users")]
    public class UserEntity : BaseEntity
    {
        [Column("first_name")]
        public string FirstName { get; set; }
        [Column("last_name")]
        public string LastName { get; set; }
        [Column("age")]
        public int Age { get; set; }

        public UserEntity()
        {
            CreateDate = DateTime.Now;
        }
    }
}
