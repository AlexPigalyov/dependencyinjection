﻿using Autofac;
using IoCExample.Data;
using IoCExample.Data.Repositories;
using IoCExample.Data.Repositories.Abstract;
using IoCExample.Serivces;
using IoCExample.Serivces.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoCExample
{
    public class AutofacConfig
    {
        public static IContainer ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<UserService>().As<IUserService>();

            builder.RegisterType<UserRepository>().As<IUserRepository>();

            builder.RegisterType<EFDbContext>().AsSelf();

            return builder.Build();
        }
    }
}
