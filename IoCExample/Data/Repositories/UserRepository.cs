﻿using IoCExample.Data.Repositories.Abstract;
using IoCExample.Entities;

namespace IoCExample.Data.Repositories
{
    public class UserRepository : BaseRepository<UserEntity>, IUserRepository
    {
        private readonly EFDbContext _context;
        public UserRepository(EFDbContext context) : base(context)
        {
            _context = context;
        }

        public UserEntity FindById(int id)
        {
            return _context.Users.Find(id);
        }
    }


}
