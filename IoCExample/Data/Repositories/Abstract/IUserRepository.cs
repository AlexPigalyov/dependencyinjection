﻿using IoCExample.Entities;

namespace IoCExample.Data.Repositories.Abstract
{

    public interface IUserRepository : ICRUDRepository<UserEntity>
    {
        UserEntity FindById(int id);
    }
}
