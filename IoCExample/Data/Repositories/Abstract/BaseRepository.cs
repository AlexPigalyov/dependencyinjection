﻿using IoCExample.Entities.Abstract;
using System.Collections.Generic;
using System.Data.Entity;

namespace IoCExample.Data.Repositories.Abstract
{
    public class BaseRepository<T> : ICRUDRepository<T> where T : BaseEntity
    {
        private EFDbContext _context;
        private DbSet<T> _dbSet;

        public BaseRepository(EFDbContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public void Add(T entity)
        {
            _dbSet.Add(entity);
            _context.SaveChangesAsync();
        }

        public void Delete(T entity)
        {
            _dbSet.Remove(entity);
            _context.SaveChangesAsync();
        }

        public IEnumerable<T> Get()
        {
            return _dbSet;
        }
    }
}
