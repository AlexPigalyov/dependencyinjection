﻿using IoCExample.Entities.Abstract;
using System.Collections.Generic;

namespace IoCExample.Data.Repositories.Abstract
{
    public interface ICRUDRepository<T> where T : BaseEntity
    {
        void Add(T entity);
        IEnumerable<T> Get();
        void Delete(T entity);
    }
}
