﻿using IoCExample.Entities;
using SQLite.CodeFirst;
using System.Data.Entity;

namespace IoCExample.Data
{
    public class EFDbContext : DbContext
    {
        public EFDbContext() : base("ConnectionString")
        {

        }

        public DbSet<UserEntity> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var sqliteConnectionInitializer = new SqliteCreateDatabaseIfNotExists<EFDbContext>(modelBuilder);
            Database.SetInitializer(sqliteConnectionInitializer);
        }
    }
}
