﻿using Autofac;
using IoCExample.Data;
using IoCExample.Data.Repositories;
using IoCExample.Domain;
using IoCExample.Serivces;
using IoCExample.Serivces.Abstract;
using System;

namespace IoCExample
{
    class Program
    {

        static void Main(string[] args)
        {
            IContainer container = AutofacConfig.ConfigureContainer();

            IUserService userService = container.Resolve<IUserService>();

            Console.Write("Введите ваше имя: ");
            string firstName = Console.ReadLine();

            Console.Write("Введите вашу фамилию: ");
            string lastName = Console.ReadLine();

            Console.Write("Введите сколько вам лет: ");
            int age = int.Parse(Console.ReadLine());

            User newUser = new User
            {
                FirstName = firstName,
                LastName = lastName,
                Age = age
            };

            userService.Add(newUser);
        }
    }
}
