﻿using Autofac;
using IoCExample.Data.Repositories.Abstract;
using IoCExample.Domain;
using IoCExample.Entities;
using IoCExample.Serivces.Abstract;
using Mapster;
namespace IoCExample.Serivces
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;
        public UserService()
        {
            IContainer container = AutofacConfig.ConfigureContainer();

            _userRepository = container.Resolve<IUserRepository>();
        }

        public void Add(User user)
        {
            _userRepository.Add(user.Adapt<UserEntity>());
        }
    }
}
