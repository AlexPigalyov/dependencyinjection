﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoCExample.Domain
{
    public class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}
